# Task #5
List up to 5 ways to make an application start automatically after a Windows server boots

## 1. Windows Services Manager
Windows Services Manager is a computer program that operates in the background. 
cmd : services.msc
Windows: "Computer" -> “Manage” -> “Services and Applications” -> “Services”

To change the way a Windows service starts, you must first open its Properties. 
To do that, right-click on the service and then on Properties. the option to modify its startup type.


## 2. StartUp Links
Create the shortcut of the program you want to run in startup.
Then drop this shortcut at C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup

## 3. Regiester
The list of services is in a branch of the Register of Windows 
`HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services`

value of option «Start» has type «REG_DWORD» и can accept values:
2 - Auto
3 — Demand
4 — Disable

## 4. Net
Management of services is possible by means of a command line:
net stop service_name
net start service_name

Setting the Service Startup Mode
sc config "service_name" start="option"

option can be:

* auto 
* demand
* disabled

## 5. Powershell Set-Service
Windows PowerShell can manage Windows services follow cmd changes the startup type of services :

Set-Service -Name "ServiceName" -StartupType Automatic
