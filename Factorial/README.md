# Task #1
Fix the code:

```
def factorial(n):
 """
 @returns: factorial of n
 """
 acc = 1
 for i in range(n):
    acc *= i
 return acc
```

for gettting a result use the following cmd:

`python main.py`

for testing:

`pytest ./test`

python must be 3.6+ 

The code has one mistake. range(n) returns list [0,...,n-1] that way
result will be 0.
We should generate list [1,...,n] , therefore range(1, n+1)