import sys
import os

fact_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

sys.path.append(fact_dir)
from factorial.main import factorial


def test_answer():
    print("Run_test:")
    assert factorial(0) == 1
    assert factorial(1) == 1
    assert factorial(5) == 120
    assert factorial(10) == 3628800


