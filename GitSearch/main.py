from git import Repo
import click
import os
import json
import GitSearch

LOG = GitSearch.LOG


def search_json(repo: Repo) -> str:
    """
     Try to find first commit in given GIT repo that introduces invalid JSON syntax.

    1. Gets all commits of the repository
    2. Runs through the list of commits  in reverse order
    3. In each commit, it looks at the files  to find a json file.
    4. Gets snapshot of the file
    5. Checking syntax. If it gets exception of json.JSONDecodeError, then it found the file.


    :param repo: Repo instance pointing to the git repository
    :return: If True return hash of commit else empty string.
    """
    LOG.info("<< Start >>")
    commit_hash = repo.git.log("--all", "--pretty=format:%H", "--follow", "*.json").split("\n")
    LOG.debug(f"Count commits: {len(commit_hash)}",)
    for commit in commit_hash[::-1]:
        files = repo.git.show("--pretty=format:", "--name-only", f"{commit}").split("\n")
        for file in files:
            basename, suffix = os.path.splitext(file)
            if suffix == ".json":
                try:
                    stream = repo.git.show(f"{commit}:{file}")
                    json.loads(stream)
                except json.JSONDecodeError as ex:
                    LOG.debug(ex)
                    return commit
                except FileNotFoundError as ex:
                    LOG.debug("FileNotFoundError: ", ex)
                except Exception as ex:
                    LOG.debug("Except: ", ex)
                    continue
    return ''


@click.command()
@click.option('-v', '--verbose', is_flag=True)
@click.option('-l', '--log', type=str, default=None,
              help="Name of the file to append log information to.")
@click.option('-r', '--repo-dir', default=".", type=str)
def cli(verbose, log, repo_dir):
    """Enter point of the module."""
    if verbose is True:
        _verbose = 3
    else:
        _verbose = 1
    GitSearch.configure_logging(verbosity=_verbose, filename=log)
    repo = Repo(repo_dir)
    commit = search_json(repo)
    LOG.info(commit)


if __name__ == '__main__':
    cli()