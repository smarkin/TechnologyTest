# Task #4

Provide a script that reboots a Linux server in case if system has less than 13% of RAM free

for checking RAM free use the script checkRAM.sh

checkRAM.sh has one argument threshold value of RAM. default is 0.13
```
> checkRAM.sh 0.20
> checkRAM.sh 0.50
```

Scripts uses zsh and bc, for installing their use this cmd:
`apt-get install zsh bc`
