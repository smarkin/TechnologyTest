#!/usr/bin/zsh

ARG1=${1:-0.13}

Total=`free -m | awk '{if(NR==2) print $2 }'`
Free=`free -m | awk '{if(NR==2) print $4}'`

echo "Total: $Total"
echo "Free: $Free"
reserve=$( bc <<< "scale=4; (1.0 - $Free/$Total) " )

if (( $(bc -l <<< "$reserve < $ARG1 ")  ));
then
 echo "Reserve: $reserve"
 echo "shutdown -r"
fi