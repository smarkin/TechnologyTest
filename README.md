The list in what directory lies the solution of a task

1.  Fix the code: 

>    ./Factorial  
    
2. Write a script that sets the text of "timeout" element to "15s" in XML file with the following content:

>   ./XMLScript
    
3. List the ways you know to package and distribute custom software (e.g. your web application)

>    ./Maintainer
    
4. Provide a script that reboots a Linux server in case if system has less than 13% of RAM free

>   ./RAMWarning    
    
5. List up to 5 ways to make an application start automatically after a Windows server boots

>    ./AutoStartWin
    
6. Write a script that finds the first commit in given GIT repo that introduces invalid JSON syntax to any "*.json" file in the root repo folder

>   ./GitSearch
