# Task #2
Write a script that sets the text of "timeout" element to "15s" in XML file with the following content:

```
<site_config> 
 <timeout/>
</site_config>
```

for getting a result use one the following cmds:

`sed -r -n 's/timeout/timeout15s/gw a.out'  ./data/config.xml`

`sed -r 's/timeout/timeout15s/g'  ./data/config.xml` > a.out

`sed -r -i 's/timeout/timeout15s/g'  ./data/config.xml`